#
# deploy.sh
#
# Copyright (c) 2018 Giorgos Kappes <giorgos@giorgoskappes.com>  
#
# This bash script is used to deploy a public version of the site.
# It calculates the total size of each html file and inlines the
# full stylesheet if the total file size does not exceed a
# default threshold. Otherwise, it links the stylesheet to the
# html file. Additionally, it minimizes the stylesheet, the html,
# and the javascript files and compresses them. 
#
NPM="npm"
MINIFY_HTML="html-minifier"
MINIFY_CSS="csso"
MINIFY_JS="uglifyjs"
SCP="scp"
CAT="cat"
GZIP="gzip"
WC="wc"
CUT="cut"
TO="$1"
INLINE_MAX="28672"
DEPS="$MINIFY_HTML $MINIFY_CSS $MINIFY_JS $GZIP $WC $CUT"

check_deps()
{
    if [ "$TO" = "scylla.cs.uoi.gr" ]; then
	DEPS+=" $SCP"
    fi

    fail=0
    for i in $DEPS; do
	if ! hash $i; then
	    echo "$i is missing."
	    fail=1
	fi
    done
    if [ $fail -eq 1 ]; then
	echo "Install the missing dependencies and try again."
	exit 1
    fi
}

deploy()
{
    # Create a temporary public directory.
    echo "[1/7] Creating a temporary working directory..."
    rm -rf .public
    mkdir .public
    cp -r {css,js,images,files,robots.txt,sitemap.xml} .public/.

    # Minify css.
    for f in .public/css/*.css; do	
	echo "[2/7] Minifying CSS file: $f..."
	csso $f --output $f-min
	mv $f-min $f
    done;
    cp .public/css/*.css .public/.
    rm -rf .public/css

    # Generate htmls.
    cp html/404.html .public/.
    for f0 in html/_0_*.html; do
	f1=$(echo $f0 | sed -r 's/_0_/_1_/')
	f=${f0##*/}
	f=${f##_0_}
	echo "[3/7] Generating HTML file: $f..."
	SS=$(${WC} -c .public/style.css | cut -d" " -f1)
	S0=$(${WC} -c $f0 | cut -d" " -f1)
	S1=$(${WC} -c $f1 | cut -d" " -f1)
	SIZE=$((S0+S1+SS))	
	if [ "$SIZE" -lt "$INLINE_MAX" ]; then
	    echo "[3/7] Inlined CSS in HTML file $f. Total size is: $SIZE."
	    cat $f0 > .public/$f
	    echo "<style  type=\"text/css\">" >> .public/$f
	    cat .public/style.css >> .public/$f
	    echo -e "\n</style>" >> .public/$f
	    cat $f1 >> .public/$f
	else
	    echo "[3/7] Not inlined CSS in HTML file $f. Total size is $SIZE."
	    cat $f0 > .public/$f
	    echo "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" media=\"screen\" />" >> .public/$f
	    cat $f1 >> .public/$f
	fi
    done
    
    # Minify html.
    for f in .public/*.html; do	
	echo "[4/7] Minifying HTML file: $f..."
	html-minifier --remove-comments --keep-closing-slash $f -o $f-min
	mv $f-min $f
    done;

    # Minify javascript.
    for f in .public/js/*.js; do
	echo "[5/7] Minifying Javascript file: $f..."
	uglifyjs $f --output $f-min
	mv $f-min $f
    done;

    # Compress html, css, and javascript.
    echo "[6/7] Performing file compression..."
    gzip -k -6 .public/*.html
    gzip -k -6 .public/*.css
    gzip -k -6 -r .public/js
    
    # Deploy the site.
    echo "[7/7] Deploying the site into the public directory..."
    if [ "$TO" = "gitlab.com" ] || [ "$TO" = "local" ]; then
	mv .public public
    else
	scp -r .public/* gkappes@$TO:~/public_html/.
    fi
    echo "Done."
}

if [ "$TO" = "" ]; then
    echo "Run as: ./deploy.sh address"
    exit 1
fi

check_deps
deploy

    

